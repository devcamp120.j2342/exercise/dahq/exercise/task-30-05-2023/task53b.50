import model.Account;
import model.Customer;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Nguyen Van A", 10);
        Customer customer2 = new Customer(2, "Nguyen Van B", 20);

        System.out.println("Customer1: " + customer1.toString());
        System.out.println("Customer2: " + customer2.toString());

        Account account1 = new Account(3, customer1, 1000);
        Account account2 = new Account(4, customer2, 2500);
        System.out.println("account1: " + account1.toString());
        System.out.println("Account2: " + account2.toString());

        Account account = account1.deposit(10);
        System.out.println(account.toString());
        Account account3 = account.withdraw(200);
        System.out.println(account3.toString());

    }
}
