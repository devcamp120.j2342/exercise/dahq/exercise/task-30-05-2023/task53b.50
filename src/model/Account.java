package model;

public class Account {
    private int id;
    private Customer customer;
    private double balance = 0.0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }

    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }

    @Override
    public String toString() {
        return customer.toString() + " ,id: " + this.id + " ,balance: " + this.balance + "$";
    }

    public Account deposit(double amount) {
        return new Account(this.id, this.customer, amount + balance);
    }

    public Account withdraw(double amount) {
        Account account = new Account(id, customer, balance);
        if (this.balance > amount) {
            account = new Account(this.id, this.customer, this.balance - amount);
        } else {
            System.out.println("amount withdraw exceed the current balance!");
        }
        return account;

    }

}
